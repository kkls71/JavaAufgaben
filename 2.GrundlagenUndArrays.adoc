= Grundlagen und Arrays
Dominikus Herzberg und Christopher Schölzel
:toc: left
:toctitle: Inhaltsverzeichnis
:toclevels: 4
:icons: font
:stylesheet: italian-pop.asciidoc.css
:sourcedir: ./code/2


include::prelude.adoc[]

== Begrifflichkeiten und Grundlagen

=== Integersuffix

Das Integersuffix `L` steht für:

- [ ] light
- [ ] little
- [ ] long
- [ ] low

=== Ganzzahl-Literal

Ein Ganzzahl-Literal ohne Suffix ist vom Typ

- [ ] byte
- [ ] short
- [ ] int
- [ ] float

=== Byte

Ein Byte sind

- [ ] 2 Bits
- [ ] 4 Bits
- [ ] 6 Bits
- [ ] 8 Bits
- [ ] 10 Bits

=== Bit

Ein Bit ist

- [ ] `true` bzw. `false`
- [ ] `1` bzw. `0`
- [ ] `1` bzw. `2`
- [ ] `+` bzw. `-`

=== Primitive Typen

Bitte geben Sie für alle acht primitiven Typen die Anzahl der benötigten Bytes und die verwendete Kodierung an.

Versuchen Sie sich zu erinnern oder raten Sie. Nachschlagen bringt wenig zur Reflektion Ihres Wissens.

== Literale

IMPORTANT: Verzichten Sie bei den folgenden Aufgaben bitte im ersten Schritt auf technische Hilfsmittel zum Umrechnen von Zahlen. Im zweiten Schritt bestätigen Sie sich Ihr Ergebnis mit der JShell, soweit das möglich ist.

=== Zahlensysteme

Geben Sie zu den folgenden Dezimalzahlen die Literale an im Binär-, im Oktal- und im Hexadezimal-Format.

* 10
* 256
* 123

Fragen Sie Wikipedia, wenn Sie wissen wollen, was das https://de.wikipedia.org/wiki/Hexadezimalsystem[Hexadezimalsystem], das https://de.wikipedia.org/wiki/Oktalsystem[Oktalsystem] und das https://de.wikipedia.org/wiki/Dualsystem[Binärsystem] ist.

=== Zweierkomplement

1. Wandeln Sie die Zahlen 120 und -17 um in eine Binärkodierung mit 8 Bits im Zweierkomplement.
2. Welchen Dezimalzahlen entsprechen die folgenden Binärzahlen aus 8 Bits kodiert im Zweierkomplement: 01101100, 10100101, 11111111, 00001111?

Wikipedia hilft Ihnen weiter, wenn Sie nicht viel anzufangen wissen mit dem https://de.wikipedia.org/wiki/Zweierkomplement[Zweierkomplement].

=== Hexadezimale Zahl

Dem Literal `0xCAFE` entspricht welcher Dezimalwert?

=== Null-Literal

Wie wird das Null-Literal geschrieben?

- [ ] `Null`
- [ ] `NULL`
- [ ] `null`
- [ ] `0`

=== Klassen-Literal

Java kennt ein Klassenliteral. Das ist eine Recherche-Aufgabe: Wie lautet das Klassenliteral?

IMPORTANT: Es ist nicht `class` zur Deklaration einer Klasse gesucht.

Suchen Sie danach. Wo haben Sie's gefunden?

include::preDetailsSolution.adoc[]
Das Literal lautet `class`, es ist aber nicht das Schlüsselwort für die Deklaration einer Klasse gemeint, sondern `.class`.

----
jshell> class A {}
|  created class A

jshell> A.class
$3 ==> class A
----
include::postDetails.adoc[]

== JShell

Starten Sie eine Konsole. Unter Windows geht das z.B. mit der Windows-Taste + `r`; im sich öffnenden Fenster zur Ausführung tippen Sie `cmd` (für _command_) ein. In der Konsole starten Sie mit dem Aufruf `jshell` die JShell, sofern alles bei Ihnen richtig konfiguriert ist.

=== Einfache Kommandos in der JShell

Finden Sie heraus, wie man in der JShell

* eine Datei lädt (man sagt auch "öffnet"),
* sich anzeigen lassen kann, was man bisher dort alles eingegeben hat,
* alle Eingaben zurücksetzt,
* die JShell beendet.

=== Datei erzeugen und laden

NOTE: Der Sinn der folgenden Aufgabe ist, zu klären, ob Sie grundlegend damit zurecht kommen, eine Datei anzulegen und dafür zu sorgen, dass Datei und JShell innerhalb desselben Verzeichnisses arbeiten. Sollten Sie Schwierigkeiten haben, so ist das absolut keine Schande. Sie sind zum Lernen hier! Lassen Sie sich von einem Studienkollegen oder einer Studienkollegin helfen!

Tippen Sie in der JShell `2 + 3` ein. Was sehen Sie?

Legen Sie nun mit einem Editor Ihrer Wahl eine Datei an, in die Sie als Text die Rechnung `10 - 3` eingeben. Speichern Sie die Datei unter dem Namen `calc.txt`.

Laden Sie die Datei in der JShell. Was sehen Sie?

TIP: Verwenden Sie als Editoren beispielsweise https://atom.io/[Atom] oder https://code.visualstudio.com[Visual Studio Code].

=== Primitive Typen

Zähle alle acht primitiven Typen von Java auf. Gebe außerdem den benötigen Speicher für einen Wert des Typs und die verwendete Kodierung an.

=== Begriffe

Erklären Sie die folgenden Begriffe möglichst exakt.

* Literal
* Ausdruck
* Deklaration
* Zuweisung
* Auswertung
* Operator
* Typ
* Initialisierung
* Variable

=== Unär, Binär, Ternär, Zuschwär?

Sie haben in der Vorlesung unäre und binäre Operatoren von Java kennengelernt. Java hat aber auch einen ternären Operator.

. Wie unterscheidet sich ein ternärer Operator von einem unären oder binären? Was bedeuten diese Worte?

. Was ist die Syntax für den ternären Operator in Java?

. Was ist die Semantik des ternären Operators in Java?

. Geben Sie mindestens zwei beispielhafte Ausdrücke (besser vier) an, die den ternären Operator verwenden!

== Variablen

=== Deklaration einer Variablen

Deklarieren und initialisieren Sie in der JShell

* eine Variable vom Typ `int` mit dem Wert -20.
* eine Variable vom Typ `byte` mit dem Wert 7.
* eine Variable vom Typ `char` mit dem Zeichen `a`.

include::preDetailsSolution.adoc[]
----
jshell> int i = -20
i ==> -20

jshell> byte b = 7
b ==> 7

jshell> char c = 'a'
c ==> 'a'
----
include::postDetails.adoc[]

=== Deklaration zweier Variablen

Deklarieren und initialisieren Sie in der JShell die Variablen `x` und `y` vom Typ `float` auf denselben Wert, nämlich die Zahl Sieben -- und zwar in kürzester Schreibweise.

include::preDetailsSolution.adoc[]
Mit einer Anweisung, einer kombinierten Deklaration für zwei Variablen, ist die Aufgabe lösbar. Die Lösung hat einen möglichen Nachteil: Der Initialwert, der für beide Variablen gleich sein soll, muss zweimal hingeschrieben werden. Das hat das Potenzial eines Schreibfehlers.

.Kurze Lösung: Doppelte Angabe des Zuweisungswerts
----
jshell> float x = 7.0f, y = 7.0f
x ==> 7.0
y ==> 7.0
----

Mit zwei Codezeilen ist es einfach: Erst werden die Variablen deklariert, dann wird ihnen ein und derselbe Wert zugewiesen. Aber es geht auch kürzer. 

."Sichere" Lösung: Garantiert gleicher Zuweisungswert
----
jshell> float x, y
x ==> 0.0
y ==> 0.0

jshell> x = y = 7.0f
x ==> 7.0

jshell> y
y ==> 7.0
----

Die folgende Lösung ist die kürzeste, die dazu den Nachteil vermeidet, den Initialwert zweimal hinschreiben zu müssen. Allerdings ist sie selten in "echtem" Code zu sehen. Die meisten Programmierer(innen) verwenden dafür zwei getrennte Deklarationen.

.Musterlösung: 
----
jshell> float x = 7.0f, y = x
x ==> 7.0
y ==> 7.0
----
include::postDetails.adoc[]

=== Blöcke

Erklären Sie den Fehler, der in dem folgenden Codestück auftritt.

----
int x;
{
    int y = 3;
    x = 10;
}
System.out.printf("x = %d", x);
System.out.printf("y = %d", y);
----

include::preDetailsSolution.adoc[]
Man kann in Java an jeder beliebigen Stelle, an der man eine Anweisung (z.B. ein Aufruf einer Methode oder eine Variablenzuweisung) schreiben kann, einen Block in geschweiften Klammern definieren. Dieser Block hat keine Auswirkung außer, dass er die _Lebensdauer_ von Variablen begrenzt, die innerhalb dieses Blocks definiert werden. In diesem Beispiel ist die Lebensdauer der Variable `y` auf den Inhalt des Blocks begrenzt. Nachdem der Block geschlossen wurde, existiert die Variable nicht mehr und kann dementsprechend auch nicht mehr ausgegeben werden. Gleiches gilt auch für Variablen in Blöcken, die Teil einer If- oder For-Anweisung sind.
include::postDetails.adoc[]

== Berechnungen mit "Character"

=== Plus 1

Was ist der Wert der Variable `c` in folgendem Codebeispiel?
Können Sie das Ergebnis erklären?

  char c = 'a' + 1;

=== Nochmal plus 1

Führen Sie den folgenden Code in der JShell aus. Erklären sie das Ergebnis.

  char c = 0xffff;
  c += 1;

=== Unicode

Wie viel Bit hat ein `char` in Java? Reicht das, um alle Zeichen des Unicode-Standards darzustellen? Falls nein, was muss man tun, um in Java eines dieser sonst nicht darstellbaren Zeichen auszudrücken?

== Rechenprobleme

=== Da hat Java in der Schule wohl nicht aufgepasst

Die folgenden Ausdrücke ergeben nicht den mathematisch richtigen Wert in Java. Warum?

* `2147483647 * 3`
* `16777215 * 10.0f / 10.0f`
* `0.1 + 0.2`
* `-2147483648 - 2147483647`

include::preDetailsSolution.adoc[]
Das Rechnen mit Zahlen im Computer folgt ein wenig anderen "Gesetzen" als denen, die Sie in der Schule kennengelernt haben. Das zeigt Ihnen auch das Video https://www.youtube.com/watch?v=nzPvVWHgL2U[Digitale Raumkrümmung und das Zweierkomplement].
include::postDetails.adoc[]

=== Genauigkeit von Fließkommazahlen

Welche der folgenden Ausdrücke ergibt den Wert `true`, welcher den Wert `false`? Warum?

* `1 + 1 + 1 == 3`
* `1.0 + 1.0 + 1.0 == 3.0`
* `0.1 + 0.1 + 0.1 == 0.3`
* `0.01 + 0.01 + 0.01 == 0.03`

include::preDetailsSolution.adoc[]
Bei `0.1 + 0.1 + 0.1 == 0.3` kommt `false` heraus, alle anderen Ausdrücke ergeben `true`.

Um das Problem anzudeuten: Die Zahl `0.1` hat, wenn man sie ins Binärsystem umwandelt, unendliche viele Nachkommastellen; es ergibt sich ein periodisch wiederholendes Muster. Als `float` wird `0.1` nach IEE754 kodiert mit einem Vorzeichen (erstes Bit ganz links), Exponent (die folgenden acht Bits) und Mantisse (die restlichen 23 Bits):
----
00111101 11001100 11001100 11001101
----
Man sieht, dass die Periode `0011` am Ende "abgeschnitten" wird. Dieser "verkürzten" Binärrepräsentation entspricht dezimal die Zahl
----
1.00000001490116119384765625E-1
----
Manchmal, wie bei `0.01` gleichen sich die Ungenauigkeiten bei der Berechnung wiederum aus und das "korrekte" Ergebnis kommt heraus.
include::postDetails.adoc[]

=== Wert und Datentyp von Ausdrücken

Welchen Wert (als Java-Literal geschrieben) ergeben die folgenden Java-Ausdrücke? Welchen Datentyp haben sie? Schreiben Sie sich die Antworten zunächst auf ein Blatt Papier und überprüfen Sie diese dann mit Hilfe der JShell. Versuchen Sie, jedes Ergebnis zu erklären.

* `13 % 10`
* `5 + 10`
* `5 / 10`
* `5.0 / 10`
* `"5" + 10`
* `1.0 / 0.0`
* `0.0 / 0.0`
* `4.0f + 1e-10f`
* `4.0 + 1e-10`

=== Pingeliger Rechner

Welche der folgenden Java-Ausdrücke sind korrekt, bei welchen "beschwert" sich der Rechner und warum tut er das? Was muss jeweils an den Ausdrücken verändert werden, damit sie gültig werden? Schreiben Sie Ihre Antworten erst auf ein Blatt Papier und probieren Sie dann aus, was die JShell Ihnen sagt.

* `int x = 10`
* `float x = 10;`
* `int x = 10.0;`
* `int x = 10.5;`
* `String s = "5";`
* `String s = 5;`
* `int x = "5";`
* `String s = 5 + "0";`
* `String s = "5" + 0;`
* `int x = 1 + 0.0;`
* `boolean b = 10 < 3 & true;`

=== Vollständige Klammerung

Bei den folgenden Ausdrücken geht es darum: In welcher Reihenfolge werden welche Operationen vor welcher anderen Operation ausgeführt. Machen Sie das deutlich, indem Sie Klammern setzen, so dass die Ausdrücke vollständig geklammert sind. Der geklammerte und der originale Ausdruck müssen das gleiche Ergebnis liefern.

* `3 + 5 * 7 + 9`
* `3 + 5 + 7 + 9`
* `7 & 10 + 3 & 0xff`
* `a == 42 || a > 10 && a < 30`
* `a = 7 + 9 < 3`
* `a = 4 || a = 7` *(Achtung: Was ist hier wirklich gemeint?)*
* `a = b = c = 15`

Hinweis: Gehen Sie davon aus, dass die verwendeten Variablen deklariert sind.

=== Tilde

Erklären Sie, warum `~7` als Ergebnis `-8` ergibt?

include::preDetailsSolution.adoc[]
Die Tilde `~` ist der sogenannte Not-Operator: Die Bits in der Binärkodierung werden invertiert.

----
jshell> Integer.toBinaryString(7)
$49 ==> "111"

jshell> Integer.toBinaryString(~7)
$50 ==> "11111111111111111111111111111000"
----

Da ein Integer im Zweierkomplement kodiert ist, entspricht `$50` Dezimal repräsentiert einer `-8`.
include::postDetails.adoc[]

== Ausdrücke finden

=== Gerader oder ungerader Zufall?

Das folgende Programmfragment erzeugt eine Zufallszahl von `0` bis `9` und speichert deren Wert in der Variable `a`.

    Random r = new Random();
    int a = r.nextInt(10);

Formulieren Sie einen Ausdruck, der `true` ergibt, wenn `a` gerade ist, und `false`, wenn `a` ungerade ist.

include::preDetailsSolution.adoc[]
----
a % 2 != 1
----
include::postDetails.adoc[]

=== Gerader Zufall

Formulieren Sie einen Ausdruck, der ausschließlich gerade Zufallszahlen vom Typ `int` im Zahlenbereich von `0` bis `10` (jeweils einschließlich) erzeugt. Nutzen Sie den Code der vorigen Aufgabe als Anregung und Ausgangspunkt.

include::preDetailsSolution.adoc[]
int a = r.nextInt(6) * 2;
include::postDetails.adoc[]

=== Zufallszahlen

Was glauben Sie: Sind die Zufallszahlen, die wir hier aus einer Java-Bibliothek nutzen wirklich zufällig? Falls ja, wo kommt der Zufall her? Falls nein, warum sehen sie zufällig aus?

=== Variablentausch

Tauschen sie den Wert der beiden Variablen `a` und `b` in dem folgenden Programmfragment mit so wenigen Java-Befehlen wie möglich.

    Random r = new Random();
    int a = r.nextInt(10);
    int b = r.nextInt(10);

include::preDetailsSolution.adoc[]
Gemeint ist eine Lösung wie:
----
int tmp = a;
a = b;
b = tmp;
----

Man kann das auch als Zweizeiler realisieren. Die folgende Lösung ist jedoch schlechter Code, weil sie auf drei(!) Seiteneffekte während der Addition setzt, wobei die Addition selbst keine Rolle spielt. Schlimmer geht's kaum!
----
int tmp;
(tmp = a) + (a = b) + (b = tmp);
----
include::postDetails.adoc[]

== In Einsen und Nullen gedacht

Die folgende Aufgabe ist kniffelig und eine Herausforderung. Ihr Spürsinn für Details auf binärer Ebene ist gefragt.

=== Binärer Logarithmus

Ein (ziemlich verrückt wirkender) Student aus einem höheren Semester behauptet, er habe einen Weg gefunden, mit ein paar einfachen Rechenoperationen den 2er-Logarithmus einer Fließkommazahl zu berechnen, den _logarithmus dualis_ (ld). Zum Beweis gibt er Ihnen den folgenden Code.

  float x = 8;
  int x_i = Float.floatToIntBits(x);
  int ld = ((x_i & 0x7f800000) >> 23) - 127;

Die Zahl, von der der Logarithmus bestimmt werden soll, ist `x`, das Ergebnis ist `ld`.

Er behauptet, dass dieser Code für jeden Wert der Variablen `x` funktioniert und sagt, dass er gar nicht versteht, warum irgendjemand Bibliotheksfunktionen für so eine einfache Rechnung braucht. Dann brabbelt er noch etwas davon, dass C die beste Programmiersprache sei und dass das ja heute kein Ersti mehr verstehen würde.

Beweisen Sie dem Studenten das Gegenteil! Können Sie erklären, warum sein Code für manche Zahlen ganz gut funktioniert, für andere aber doch eher ungenau ist?

== Verzweigungen

=== if mit else/else if

==== Warnanzeige

Eine einfache Warnanzeige blinkt, oder sie blinkt nicht. Das sei durch die boolsche Variable `isOn` (_true_ für blinkend) abgebildet. Auf der Konsole sei im einen Fall "flashes", im anderen Fall "does't flash" ausgegeben.

* Verwenden Sie eine if-Anweisung
* Verwenden Sie keine if-Anweisung (auch kein `switch`)

include::preDetailsSolution.adoc[]
.Lösung mit if:
----
jshell> boolean isOn;
isOn ==> false

jshell> if (isOn) System.out.printf("flashes"); else System.out.printf("doesn't flash");
doesn't flash
----

.Lösung ohne if
----
jshell> isOn = true;
isOn ==> true

jshell> System.out.printf(isOn ? "flashes" : "doesn't flash");
flashes
----
include::postDetails.adoc[]

==== Datenauswertung

Abhängig vom Wert der double-Variablen `d` setzen Sie den Wert der String-Variablen `res` auf

* " >1", wenn der Wert größer 1 ist
* "\<=1", wenn der Wert kleiner oder gleich 1 ist
* "==0", wenn der Wert gleich 0 ist; ignoriere den vorigen Fall

include::preDetailsSolution.adoc[]
[source,java]
----
double d = 2.3,
String res = "";
if (d == 0) res = "==0";
else if (d > 1) res = ">1";
else res = "<=1";
----
include::postDetails.adoc[]

=== PQ-Formel
Schreiben Sie ein Programmfragment, dass für die unten angegebenen Variablen `p` und `q` das Ergebnis der sogenannten https://de.wikipedia.org/wiki/Quadratische_Gleichung#p-q-Formel[p-q-Formel] berechnet. Unterscheiden Sie per `if`-Anweisung (samt `else if`), ob es eine, zwei oder keine Lösung gibt. Der Nutzer soll das Ergebnis bzw. die Ergebnisse per `System.out.printf` ausgegeben bekommen. Dazu soll der Hinweis ausgegeben werden, ob es keine, eine oder zwei Lösungen gibt.

Speichern Sie das Fragment in einer Datei und laden Sie es mit dem Befehl `/open` in der JShell nach den folgenden Codezeilen, um die Berechnung durchführen zu lassen.

  Random r = new Random();
  int p = r.nextInt();
  int q = r.nextInt();

include::preDetailsSolution.adoc[]
Wenn der Term unter der Wurzel der p-q-Formel (nachfolgend `rad` genannt) Null ist, dann gibt es nur eine Lösung. Wie Sie wissen, liefern Rechnungen mit Fließkommazahlen selten exakte, meist durch kleine Rundungsfehler geringfügig ungenaue Ergebnisse. Deshalb ist es keine gute Idee, `rad == 0.0` in der `if`-Bedingung zu schreiben. Ein Schwellwert, wann wir den Absolutwert der Zahl als so gut wie Null betrachten, muss unterschritten werden.
----
double rad = Math.pow(p / 2.0, 2) - q;
if (Math.abs(rad) < 1e-10) {
  System.out.printf("eine Lösung\n");
  System.out.printf("x = %.3f\n", - p / 2.0);
} else if (rad > 0) {
  System.out.printf("zwei Lösungen\n");
  System.out.printf("x1 = %.3f\n", - p / 2.0 + Math.sqrt(rad));
  System.out.printf("x2 = %.3f\n", - p / 2.0 - Math.sqrt(rad));
} else {
  System.out.printf("keine Lösung\n");
}
----
include::postDetails.adoc[]solution[]

== Schleifen

=== Summe

Lege eine Variable `sum` an, initialisiere sie mit Null und schreibe ein Programmfragment, das die Summe der Zahlen von 1 bis 100 errechnet.

include::preDetailsSolution.adoc[]
----
int sum = 0;
for(int i = 1; i <= 100; i++) sum += i;
----
include::postDetails.adoc[]solution[]

=== Fizz
Schreiben Sie ein Programmfragment, das alle Zahlen von 1 bis 100 auf der Konsole ausgibt. Jede Zahl, die durch drei Teilbar ist soll dabei aber durch den Text "Fizz" ersetzt werden.

include::preDetailsSolution.adoc[]
----
for(int i = 1; i <= 100; i++) {
  if(i % 3 == 0) {
    System.out.printf("fizz\n");
  } else {
    System.out.printf("%d\n", i);
  }
}
----
include::postDetails.adoc[]solution[]

=== Fizzbuzz
Erweitern Sie Ihr Fizz-Programm so, dass nun auch jede durch 5 teilbare Zahl durch den Text "Buzz" ersetzt wird. Jede Zahl, die sowohl durch 3 als auch durch 5 teilbar ist, wird durch den Text "Fizzbuzz" ersetzt.

include::preDetailsSolution.adoc[]
----
for(int i = 1; i <= 100; i++) {
  String s = "";
  if (i % 3 == 0) { s += "fizz"; }
  if (i % 5 == 0) { s += "buzz"; }
  if (s.length() > 0) {
    System.out.printf("%s\n", s);
  } else {
    System.out.printf("%d\n", i);
  }
}
----
include::postDetails.adoc[]solution[]

=== 99 Bottles of Beer
Schreiben Sie ein Programm, das den Text des Liedes "99 bottles of beer" auf der Konsole ausgibt.

----
99 bottles of beer on the wall, 99 bottles of beer.
Take one down and pass it around, 98 bottles of beer on the wall.

98 bottles of beer on the wall, 98 bottles of beer.
Take one down and pass it around, 97 bottles of beer on the wall.
----
Und so weiter. Bis das Lied abschließt mit:
----
2 bottles of beer on the wall, 2 bottles of beer.
Take one down and pass it around, 1 bottle of beer on the wall.

1 bottle of beer on the wall, 1 bottle of beer.
Take one down and pass it around, no more bottles of beer on the wall.

No more bottles of beer on the wall, no more bottles of beer.
Go to the store and buy some more, 99 bottles of beer on the wall.
----

include::preDetailsSolution.adoc[]
----
//Lösung 1: Minimale Redundanz
for(int i = 99; i >= 0; i--) {
  String postfix1 = i == 1 ? "" : "s";
  String postfix2 = i == 2 ? "" : "s";
  String number1 = i == 0 ? "no more" : Integer.toString(i);
  String number2 = i == 1 ? "no more" : Integer.toString(i-1);
  System.out.printf("%1$s bottle%3$s of beer on the wall, %2$s bottle%3$s of beer.\n",
         number1.substring(0, 1).toUpperCase() + number1.substring(1),
         number1,
         postfix1);
  if (i > 0) {
    System.out.printf("Take one down and pass it around, %s bottle%s of beer on the wall.\n",
           number2,
           postfix2);
  }
}
System.out.printf("Go to the store and buy some more, 99 bottles of beer on the wall.\n");

//Lösung 2: Beste Lesbarkeit bei großer Redundanz
for(int i = 99; i > 2; i--) {
  System.out.printf("%1$d bottles of beer on the wall, %1$d bottles of beer.\n", i);
  System.out.printf("Take one down and pass it around, %d bottles of beer on the wall.\n", i-1);
}
System.out.printf("2 bottles of beer on the wall, 2 bottles of beer.\n");
System.out.printf("Go to the store and buy some more, 1 bottle of beer on the wall.\n");
System.out.printf("1 bottle of beer on the wall, 1 bottle of beer.\n");
System.out.printf("Go to the store and buy some more, 99 bottles of beer on the wall.\n");
System.out.printf("No more bottles of beer on the wall, no more bottles of beer.\n");
System.out.printf("Go to the store and buy some more, 99 bottles of beer on the wall.\n");
----

Beide Lösungen haben Vor- und Nachteile. Lösung 1 verkompliziert den Code in der Schleife eigentlich unnötig für Ausnahmen, die nur in den drei letzten Durchläufen der Schleife relevant sind. Dafür taucht der Text der Strophen auch wirklich nur ein einziges mal im Code auf. Bei Lösung 2 könnte die Redundanz dazu führen, dass man einen Tippfehler im Strophentext an bis zu 4 Stellen korrigieren müsste.

Es ist erstaunlich schwierig eine elegante lesbare Lösung zu finden, die den Liedtext trotzdem völlig korrekt ausgibt. Vielleicht finden Sie ja eine schönere Variante als diese Musterlösungen?
include::postDetails.adoc[]solution[]

== Arrays

=== Deklaration eines "regelmäßigen" Arrays

Stellen Sie sich ein rechteckiges Spielfeld vor, wie Sie es von den Brettspielen Dame oder Schach her kennen. Deklarieren Sie im ersten Schritt auf die kürzest mögliche Weise ein entsprechend großes zweidimensionales Array namens `board` vom Typ `float`. Initialisieren Sie im zweiten Schritt ausschließlich die vier Ecken mit dem Wert `3.5`.

include::preDetailsSolution.adoc[]
----
jshell> float[][] board = new float[8][8]
board ==> [[F@e320068

jshell> board[0][0] = 3.5f // <1>
$38 ==> 3.5

jshell> board[0][7] = board[7][0] = board[7][7] = 3.5f // <2>
$39 ==> 3.5
----
<1> 1. Das Suffix `f` (oder `F`) ist wichtig! 2. Der Evaluationswert der Initialisierung ist `3.5`!
<2> Diese "Kurzform" sollte man kennen.

.Lösungshinweis
Der Zuweisungsoperator `=` ist rechtsassoziativ: Die Zuweisung ganz rechts wird zuerst ausgewertet, der Seiteneffekt ist die Wertänderung des Arrays bei `[7][7]`. Das Evaluationsergebnis von `3.5` geht nun ein in die Auswertung der nächsten, links stehenden Zuweisung usw.

Aus dem Grund geht auch Folgendes:
----
jshell> 2 + (board[0][0] = 3.5f)
$40 ==> 5.5
----
Die Klammern müssen sein, da der Operator `+` höher bindet als `=`.

include::postDetails.adoc[]

=== Array-Indizes

Gegeben sei folgende Array-Initialisierung:

----
int[][] s = {{1,2},{3,4,5,6},{}}
----

Was liefern die folgenden Ausdrück zum Array zurück? Gibt es darunter Abfragen, die ungültig sind?

* `s[1][1]`
* `s[0][0]`
* `s[1][2]`
* `s[2][1]`
* `s[0][1]`
* `s[1][0]`
* `s[2]`
* `s.length`
* `s[0].length`
* `s[1].length`
* `s[2].length`

Bitte versuchen Sie das Ergebnis erst im Kopf zu ermitteln. Überprüfen Sie dann Ihr Resultat mithilfe der JShell.

=== Deklaration eines "unregelmäßigen" Arrays

Deklarieren Sie in der JShell ein zweidimensionales Array vom Typ `int` namens `scrabble`, das in der ersten Dimension drei Elemente hat, in der zweiten zwei, fünf und eins. Sie dürfen das Array lediglich deklarieren, nicht initialisieren.

Wie können Sie nach der Deklaration plausibel belegen, dass das Array in der zweiten Dimension tatsächlich nur die angegebene Anzahl an Elementen hat?

include::preDetailsSolution.adoc[]
.Zur Deklaration
----
jshell> int[][] scrabble = new int[3][]
scrabble ==> int[3][] { null, null, null }

jshell> scrabble[0] = new int[2]
$46 ==> int[2] { 0, 0 }

jshell> scrabble[1] = new int[5]
$47 ==> int[5] { 0, 0, 0, 0, 0 }

jshell> scrabble[2] = new int[1]
$48 ==> int[1] { 0 }

jshell> scrabble
scrabble ==> int[3][] { int[2] { 0, 0 }, int[5] { 0, 0, 0, 0, 0 }, int[1] { 0 } }
----

.Zur Plausibilitätskontrolle
Zeigen, dass Zugriff auf das erste und letzte Element in der zweiten Dimension möglich ist, nicht jedoch auf weiteres nach dem letzten Element.

----
jshell> scrabble[1][0]
$50 ==> 0

jshell> scrabble[1][4]
$51 ==> 0

jshell> scrabble[1][5]
|  java.lang.ArrayIndexOutOfBoundsException thrown: 5
|        at (#52:1)
----
include::postDetails.adoc[]

=== Arrays und Speicherplatz

Wieviel Speicherplatz braucht das Array `ar` mindestens im Speicher? Versuchen sie mit Ihrer Schätzung möglichst nahe an die Realität zu kommen.

----
int[][] ar = new int[10][4];
----

NOTE: Den tatsächlichen Speicherverbrauch eines Arrays in Java zu bestimmen ist sehr schwierig. Warum? Finden Sie ein paar Gründe dafür im Internet?

include::preDetailsSolution.adoc[]
Es ist nicht so entscheidend, ob Ihre Überlegungen zum Speicherbedarf des Arrays zum realen Wert führen. Es geht uns darum, ob Ihr Ansatz richtig ist und das absolute Minimum an Bytes ergibt. Folgende Rechnung:

In einem zweidimensionalen Array liefert die erste Dimension Referenzen auf -- wenn man so möchte -- zehn eindimensionale Arrays der Länge 4. Bezüglich der ersten Dimension ist die Länge zu speichern, der Wert von `ar.length`; das ist ein `int` vom Wert `10`, was 4 Bytes sind. Jede der zehn Referenzen von `ar[0]` bis `ar[9]` benötigt auf einer 32-Bit JVM ebenfalls jeweils 4 Bytes, macht in Summe 4 x 10 = 40 Bytes. Die erste Dimension fordert insgesamt einen Speicherbedarf von 44 Bytes ein.

Die zehn Arrays in der zweiten Dimension haben jeweils eine Länge zu speichern (wieder 4 Bytes, siehe z.B. `ar[0].length`) sowie vier `int`-Werte (4 x 4 = 16 Bytes). Macht 20 Bytes pro Array und insgesamt 200 Bytes für alle zusammen.

Das zweidimensionale Array `ar` belegt also im Speicher mindestens 200 + 44 = 244 Bytes.

Der reale Wert ist üblicherweise größer, da zusätzlich administrative Daten zu Verwaltung des Arrays im Speicher vorgehalten werden. Weder die Spezifikation von Java noch die der JVM geben an, wieviele Bytes eine Referenz tatsächlich belegt -- vier müssen es jedoch mindestens sein.
include::postDetails.adoc[]

=== Break

Mit der Anweisung `break;` beendet man nicht nur die Ausführung einer Switch-Anweisung, sondern man kann auch eine Schleife vorzeitig beenden.

Der folgende Code soll die Werte eines zweidimensionalen Arrays `field` auf den Wert `0` zurücksetzen bis (bei normaler "Leserichtung" in Zeilen) ein Stoppzeichen `42` erreicht ist. Leider funktioniert er nicht ganz wie gedacht. Finden Sie heraus warum und recherchieren sie eine Lösung für das Problem.

[TIP]
====
Finden Sie die genaue Syntax des `break`-Statements heraus. Man kann zwischen `break` und das Semikolon noch etwas schreiben.
====

[source,java]
----
int[][] field = {
    {1,  2,  3,  4},
    {5,  6, 42,  8},
    {9, 10, 11, 12}
}
for(int y = 0; y < field.length; y++) {
    for(int x = 0; x < field[y].length; x++) {
        if (field[y][x] == 42) break;
        field[y][x] = 0;
    }
}
----

include::preDetailsSolution.adoc[]

Es gibt in Java sogenannte _labeled statements_, mit denen man einer Anweisung eine Markierung (engl. _label_) geben kann. Diese Markierung kann dann in `break`- und `continue`-Statements verwendet werden, um anzugeben, welche Anweisung betroffen sein soll.

// link:code/4/labeled.java[] 
[source,java]
----
include::{sourcedir}/labeled.java[]
----
include::postDetails.adoc[]

=== Continue

Vervollständigen Sie die Methode `safeSum`, die die Werte eines Arrays vom Typ `double` aufsummiert, so dass Einträge mit dem Wert `NaN` ignoriert werden. Verwenden Sie dazu eine `continue`-Anweisung;

----
double safeSum(double[] ar) {
    double sum = 0;
    for(int i = 0; i < ar.length; i++) {

        sum += ar[i];
    }
}
----

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/safeSum.java[]
----
include::postDetails.adoc[]



////
Fragen:
- Fragen zum Verständnis des Skripts
- Fragen zu typischen Problemen:
  - Integer-Überlauf
  - Float-Precision
  - == vs =
  - primitiver vs. komplexer Typ
  - Math.xxx
  - Zufallszahlen wirklich zufällig? Wie kommt man ran?
  - Klammerung von Ausdrücken u. Operatorpäzedenz
- Rechercheaufgaben
- Schleife vs Stream immer als option
////


////
=== Präzedenzen

Die Operatoren in Java haben unterschiedliche Präzedenzen (Rangfolgen): In einem Ausdruck mit mehreren Operatoren werden Operationen mit höherrangigen Operatoren vor niederrangigeren Operatoren ausgewertet. Sie kennen das aus der Schulmathematik. Der Rechenausdruck `1 + 2 * 3` wird nicht von links nach rechts ausgewertet, sondern es gilt "Punkt- vor Strichrechnung". Die Multiplikation ist höherrangiger als die Addition. Deshalb ergibt sich als Ergebnis `7` und nicht `9`. Die Präzedenz kann nur mittels Klammern "ausgehebelt" werden. Der Ausdruck `(1 + 2) * 3` ergibt `9`.

Ihre Aufgabe ist, durch Ausprobieren herauszufinden, in welcher Rangfolge die folgenden Operatoren stehen:
////



