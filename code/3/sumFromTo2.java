int sumFromTo(int x, int y) {
  if(y == x) return 0;
  return x + sumFromTo(x + (x > y ? -1 : 1), y);
}