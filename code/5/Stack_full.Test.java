Stakk s = new Stakk(2, new Stakk(1, new Stakk()));
int[] n = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
int[] r = Stakk.fromArray(n).shuffle().toArray();
int[] t = Arrays.copyOf(r,r.length);
Arrays.sort(t);

boolean[] tests = {
    // constructor testing
    "-|".equals("" + new Stakk()),
    "1, -|".equals("" + new Stakk(1)),
    "1, -|".equals("" + new Stakk(1, new Stakk())),
    "2, 1, -|".equals("" + s),
    // testing size
    0 == new Stakk().size(),
    1 == new Stakk(1, new Stakk()).size(),
    2 == s.size(),
    // testing push, pop
    ((Predicate<Stakk>) s -> { s.pop(); return "1, -|".equals("" + s); }).test(s),
    1 == s.size(),
    ((Predicate<Stakk>) s -> { s.pop(); return "-|".equals("" + s); }).test(s),
    0 == s.size(),
    ((Predicate<Stakk>) s -> { s.push(1); return "1, -|".equals("" + s); }).test(s),
    1 == s.size(),
    ((Predicate<Stakk>) s -> { s.push(2); return "2, 1, -|".equals("" + s); }).test(s),
    2 == s.size(),
    // testing toArray, fromArray
    Arrays.equals(new int[]{2,1},s.toArray()),
    (s + "").equals("" + Stakk.fromArray(s.toArray())),
    // plausibility testing of shuffle
    n.length == r.length,
    !Arrays.equals(n,r),
    Arrays.equals(n,t),
}