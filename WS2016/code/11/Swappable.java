interface Swappable {
  /** Vertauscht Element an Stelle i mit Element an Stelle j */
  void swap(int i, int j);
  /** Gibt die Anzahl der Elemente zurück */
  int size();
}
