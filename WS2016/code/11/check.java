boolean check(int[] f) {
    final int[][] seqs = {{0,1,2}, {3,4,5}, {6,7,8},
                          {0,3,6}, {1,4,7}, {2,5,8},
                          {0,4,8}, {2,4,6}};
    for(int[] seq : seqs) {
        int sum = 0;
        for(int i : seq) sum += f[i];
        if (Math.abs(sum) == 3) return true;
    }
    return false;
}

