class Timer implements AutoCloseable {
  String name;
  long start = System.nanoTime();
  Timer(String name) { this.name = name; }
  public void close() {
    long time = System.nanoTime() - start;
    System.out.println(name + " took " + time + " ns");
  }
}
