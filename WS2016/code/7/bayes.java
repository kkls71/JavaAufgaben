double simulateInfectionChance(double pInfected, double pTest, int population) {
  int testPosInfected = 0;
  int testPosNormal = 0;
  for(int i=0; i < population; i++) {
    boolean infected = Math.random() < pInfected;
    boolean test = infected ? Math.random() < pTest : Math.random() < 1 - pTest;
    if (infected && test) testPosInfected++;
    else if (test) testPosNormal++;
  }
  return 1.0 * testPosInfected / (testPosNormal + testPosInfected);
}

double calculateInfectionChance(double pInfected, double pTest) {
  return pTest * pInfected / (pInfected * pTest + (1-pInfected) * (1-pTest));
}